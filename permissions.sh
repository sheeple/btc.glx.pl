#!/bin/bash

chown -R www-data:www-data *
chown -R www-data:www-data .*
find . -type f -exec chmod 0660 {} +
find . -type d -exec chmod 2770 {} +
find . -type f | while read fn; do head -n1 "$fn" | grep -q "^#\!" && echo "$fn" && chmod 770 "$fn"; done
