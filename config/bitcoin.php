<?php

/** @var array $configuration */

$configuration = [

    'directory'             =>  '/home/kruk/applications/bitcoin-0.21.0'
,   'data_directory'        =>  '/home/kruk/.bitcoin'

,   'command'               =>  [
        'version'           =>  "bitcoind -version|head -1| sed 's/^.\+v//'"
    ,   'block'             =>  [
            'count'         =>  'btc getblockcount'
        ,   'last'          =>  [
                'hash'      =>  'btc getblockcount|btc -stdin getblockhash'
            ,   'data'      =>  'btc getblockcount|btc -stdin getblockhash|btc -stdin getblock'
            ]
        ]
    ]

,   'price'                 => 'https://api.bitfinex.com/v1/pubticker/btcusd'
,   'rates'                 => 'https://api.exchangeratesapi.io/latest?base=USD&symbols=EUR,PLN'
];

return $configuration;
