<?php

if (! defined('GLX_CONSTANTS')) {
    define('GLX_CONSTANTS', true);

    define('ES', '');
    define('FS', '/');
    define('BS', '\\');
    define('CR', "\r");
    define('LF', "\x0A");
    define('NL', "\n");
    define('EOL', "\n");
    define('TAB', "\t");
    define('SPACE', ' ');
    define('NBR', ' ');
    define('DOT', '.');
    define('QM', '?');
    define('COMMA', ',');
    define('COLON', ':');
    define('DASH', '-');
    define('UNDERSCORE', '_');
    define('NA', 'N/A');
    define('PERCENT', '%');
    define('PLUS', '+');
    define('MINUS', '-');
    define('APOSTROPHE', "'");
    define('EQUAL', '=');
    define('NUL', "\0");
    define('ignore', NUL);
    define('VT', "\v");

    define('UL', "<ul>");
    define('LI', "<li>");

    define('BR', '<br/>');
    define('HR', '<hr/>');
    define('NBSP', '&nbsp;');
    define('TROY_OUNCE', 31.1034768);

    function EOT($tag) {
        return substr_replace($tag, '/', 1, 0);
    }

    define('LENNY', '( ͡° ͜ʖ ͡°)');
    define('SPAN_LENNY', "<span class='lenny'>" . LENNY . '</span>');
    define('srcDataEmptyGif', 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==');
}
