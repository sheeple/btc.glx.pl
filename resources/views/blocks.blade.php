<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha512-rO2SXEKBSICa/AfyhEK5ZqWFCOok1rcgPYfGOqtX35OyiraBg6Xa4NnBJwXgpIRoXeWjcAmcQniMhp22htDc6g==" crossorigin="anonymous" />
    <style>
        body {
            font-family: 'Nunito';
        }
    </style>
    <title>btc.glx.pl</title>
</head>
<body>
<div id="app">
    <header class="navbar fixed-top navbar-expand-sm bg-warning navbar-light" style="padding-top:5px; padding-bottom:5px">
        <div class="container container--glx">
            <a href="{{ $pageUrl ?? '/' }}" class="navbar-brand pt-0 pb-0">
                <!--div class="navbar-logo"></div-->
                <img src="/img/btc.glx.pl.svg" width="32" height="32" class="d-inline-block align-middle m-0" alt="">
                <span class="" style="">{{ config('app.name', 'glx.pl') }}</span>
            </a>

            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-label="Toggle navigation">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <div id="navbar-collapse" class="collapse navbar-collapse">
                <ul id="navbar-left" class="navbar-nav mr-auto">

                    <li class="nav-item {{ Request::is('blocks') ? 'active' : '' }}">
                        <a href="{{ route('blocks', []) }}" class="nav-link">
                            Blocks
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>

                    <li class="nav-item {{ routeActive('blocks') }}">
                        <a href="{{ route('blocks') }}" class="nav-link">
                            <i class="fa fa-globe" aria-hidden="true"></i>
                            Files
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>


                </ul>

                <ul id="navbar-right" class="navbar-nav">

                    <!--li class="nav-item">
                        <span class="d-inline-block align-middle" style="text-align:right"><small>1000 USD<br/>100000 PLN</small></span>
                    </li-->
                    @include('fragments.prices')

                    <li class="nav-item dropdown">
                        @auth
                            <a href="{{ route('user.logout') }}" role="button" id="dropdown-user" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true">
                                <span class="icon icon-user-logged"></span>
                            </a>
                        @else
                            <a href="#" role="button" id="dropdown-user" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true">
                                <span class="icon icon-user"></span>
                            </a>
                        @endauth
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-user">
                            @auth
                                @if (Route::has('user.logout'))
                                    <li><a href="{{ route('user.logout')            }}" class="dropdown-item">{{ __('Logout')           }}</a></li>
                                @endif
                            @else
                                @if (Route::has('user.authentication'))
                                    <li><a href="{{ route('user.authentication')    }}" class="dropdown-item">{{ __('Authentication')   }}</a></li>
                                @endif
                                @if (Route::has('user.registration'))
                                    <li><a href="{{ route('user.registration')      }}" class="dropdown-item">{{ __('Registration')     }}</a></li>
                                @endif
                            @endauth
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </header>

    <br/>

    <div class="container mt-5 mb-5">
        <table class="table table-striped table-bordered bg-white text-monospace font-weight-normal small">
            @foreach ($blocks as $block)
                <tr>
                    <td>{{ $block->height }}</td>
                    <td class="text-nowrap">{{ $block->created_at }}</td>
                    <td><a class="text-decoration-none" href="/block/{{ $block->hash }}">{{ $block->hash }}</a></td>
                </tr>
            @endforeach
        </table>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha512-I5TkutApDjnWuX+smLIPZNhw+LhTd8WrQhdCKsxCFRSvhFx2km8ZfEpNIhF9nq04msHhOkE8BMOBj5QE07yhMA==" crossorigin="anonymous"></script>
<script src="/js/glx.js"></script>
</body>
</html>
