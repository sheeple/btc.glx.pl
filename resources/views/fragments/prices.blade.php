@php
    $price  = json_decode(file_get_contents(config('bitcoin.price')));
    $rates  = json_decode(file_get_contents(config('bitcoin.rates')));

    $USD    = number_format($price->last_price                      , 0, DOT, SPACE);
    $PLN    = number_format($price->last_price * $rates->rates->PLN , 0, DOT, SPACE);
@endphp

<li class="nav-item">
    <span class="d-inline-block align-middle" style="text-align:right">
        <small>
            {{ $USD }} USD <br/> {{ $PLN }} PLN
        </small>
    </span>
</li>
