<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class TBtcProtocol extends Migration
{
    var $tableName = 't_btc_block';

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('protocol_id');
            $table->integer('height');
            $table->timestamp('created_at')->nullable();
            $table->binary('hash')->nullable();
            $table->integer('L');
            $table->integer('R');
            $table->unsignedBigInteger('parent_id');
            $table->binary('hash_prev');
            $table->binary('hash_next');
            $table->binary('merkle_root');
            $table->binary('chainwork');
            $table->unsignedBigInteger('difficulty');
            $table->unsignedBigInteger('nonce');
            $table->unsignedBigInteger('version');

            $table->foreign('protocol_id')
                ->references('id')->on('t_btc_protocol')
                ->onUpdate('cascade')
                ->onDelete('set null');

            $table->foreign('parent_id')
                ->references('id')->on($this->tableName)
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
