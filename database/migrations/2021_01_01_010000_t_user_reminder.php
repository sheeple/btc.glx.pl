<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class TUserReminder extends Migration
{
    var $tableName;

    function __construct()
    {
        if (empty($this->tableName))
            $this->tableName = config('auth.passwords.users.table', 't_user_reminder');

    }

    function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {

            $table->string('email')->index();
            $table->string('token')->index();
            $table->timestamp('created_at')->nullable();

            $table->foreign('email')
                  ->references('email')->on((new App\Models\User())->table())
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    function down()
    {
        Schema::drop($this->tableName);
    }
}
