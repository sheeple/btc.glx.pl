<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class TUserActivation extends Migration
{
    var $tableName = 't_user_activation';

    function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {

            $table->string('email')->index();
            $table->string('token', 64);
            $table->timestamp('created_at');

            $table->foreign('email')
                ->references('email')->on((new App\Models\User())->table())
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->primary('token');
        });
    }

    function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
