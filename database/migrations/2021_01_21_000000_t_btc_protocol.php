<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;


class TBtcProtocol extends Migration
{
    var $tableName = 't_btc_protocol';

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->integer('subsidy_decrease_block_count')->default(210000);
        });

        DB::table($this->tableName)->insert([
            [
                'name'                          => 'main'
            ]
        ,   [
                'name'                          => 'test'
            ]
        ,   [
                'name'                          => 'unittest'
            ,   'subsidy_decrease_block_count'  => 100
            ]
        ,   [
                'name'                          => 'regtest'
            ,   'subsidy_decrease_block_count'  => 150
            ]
        ]);
    }

    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
