<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class TUser extends Migration
{
    var $tableName;

    public function __construct()
    {
        $this->tableName = (new App\Models\User())->table();
    }

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('verified_at')->nullable();

            $table->boolean('activated')->default(false);
            $table->boolean('locked')->default(false);
            $table->string('password');
            $table->rememberToken();
            $table->decimal('strength', 8, 2)->default(1);

            $table->integer('parent_id')->nullable();
            $table->integer('_lft')->nullable()->index();
            $table->integer('_rgt')->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->nullable();

            $table->integer('employee_id')->nullable()->unique();

            $table->foreign('created_by')
                ->references('id')->on('t_user')
                ->onUpdate('cascade')
                ->onDelete('set null');

            $table->foreign('updated_by')
                ->references('id')->on('t_user')
                ->onUpdate('cascade')
                ->onDelete('set null');

            $table->foreign('parent_id')
                ->references('id')->on($this->tableName)
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::drop($this->tableName);
    }
}
