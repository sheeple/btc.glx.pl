<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class TCache extends Migration
{
    var $tableName;

    function __construct()
    {
        if (empty($this->tableName))
            $this->tableName = config('cache.stores.database.table', 't_cache');
    }

    function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->string('key')->unique();
            $table->text('value');
            $table->integer('expiration'); // todo: rename to expired_at
        });
    }

    function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
