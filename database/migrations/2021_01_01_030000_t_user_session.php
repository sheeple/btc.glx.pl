<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class TUserSession extends Migration
{
    var $tableName;

    function __construct()
    {
        if (empty($this->tableName))
            $this->tableName = config('session.table', 't_user_session');
    }

    function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {

            $table->string('id')->unique();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('ip_address', 45)->nullable();
            $table->text('user_agent')->nullable();
            $table->text('payload');
            $table->integer('last_activity');

            $table->foreign('user_id')
                ->references('id')->on((new App\Models\User())->table())
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
