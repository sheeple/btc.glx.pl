<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }

    public static function routes()
    {
        Route::get  ('user/authentication'      , 'Auth\LoginController@showLoginForm')->name('user.authentication');
        Route::post ('user/authentication'      , 'Auth\LoginController@login');
        Route::any  ('user/logout'              , 'Auth\LoginController@logout')->name('user.logout');

    #   Registration Routes

        Route::get  ('user/registration'        , 'Auth\RegisterController@showRegistrationForm')->name('user.registration');
        Route::post ('user/registration'        , 'Auth\RegisterController@register');
        Route::get  ('user/activation/{token}'  , 'Auth\RegisterController@activate')->name('user.activation');

    #   Password Reset Routes

        Route::get  ('password/reset'           , 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post ('password/email'           , 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

        Route::get  ('password/reset/{token}'   , 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
        Route::post ('password/reset'           , 'Auth\ResetPasswordController@reset');
    }
}
