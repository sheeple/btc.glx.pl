<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use HasFactory
    ,   Notifiable;

    protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    static
    function who()
    {
        static $user = null;

        if (is_null($user))
            if (is_null($user = Auth::user()))
                $user = static::where(DB::raw('lower(name)'), 'guest')->first(); // or die('You are nothing');

        return $user;
    }

    public function __construct(array $attributes = [])
    {
        $this->table = $this->table ? $this->table : config('auth.providers.users.table');
        parent::__construct($attributes);
    }

    public function table() {
        return $this->table;
    }
}
