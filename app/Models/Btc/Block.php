<?php

namespace App\Models\Btc;

use Illuminate\Database\Eloquent\Model;


class Block extends Model
{
    protected $table = 't_btc_block';
}
