<?php

namespace App\Models\Btc;

use Illuminate\Database\Eloquent\Model;


class BlockView extends Model
{
    protected $table = 'v_btc_block';
}
