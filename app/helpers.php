<?php

use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Events\QueryExecuted;
use Symfony\Component\VarDumper\VarDumper as Dumper;

function routeActive($currentRouteName)
{
    if (Route::currentRouteName() == $currentRouteName)
    return 'active';

    return '';
}
