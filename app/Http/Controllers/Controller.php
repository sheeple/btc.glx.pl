<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\View;


class Controller extends BaseController
{
    use AuthorizesRequests
    ,   DispatchesJobs
    ,   ValidatesRequests;

    function __construct()
    {
    #   beware: authorization has not yet been initialized
    #   see: https://laracasts.com/discuss/channels/general-discussion/laravel-53-sharing-user-variable-in-all-views?page=1
    //  View::share('user', $this->user = User::who());

        $this->middleware(function ($request, $next) {
        //	view()->share('user', $this->user = User::who());
            View::share('user', $this->user = User::who());
            return $next($request);
        });
    }
}
