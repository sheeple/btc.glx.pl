<?php

namespace App\Http\Controllers;

//use App\Models\TAd;
//use App\Models\MPTT\Noder;
//use App\Models\OLX\Category;
//use App\Models\OLX\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class IndexController extends CommonControllerAbstract
{
    protected $modelClass = 'App\Models\Btc\BlockView';

    public function index(Request $request)
    {
    //  DB::table('t_mptt2')->where('parent_id', '=', null)->get();
    //  Noder::getInstance(Category::class)->asOptionsNaturalOrder();

        $blocks = ($this->modelClass)::query()->paginate($request->amount ?? 10);

    //  $segments = explode('\\', $this->modelClass);
        $viewName = @ strtolower(end(explode(BS, $this->modelClass)));

        return view('blocks', ['blocks' => $blocks, 'viewName' => $viewName]);
    }
}
