<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


interface CommonControllerInterface
{
    public function index(Request $request);
}
